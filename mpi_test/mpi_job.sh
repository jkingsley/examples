#!/bin/bash
#SBATCH -N 4
#SBATCH -n 8

module load openmpi/4.1.3/ebae7zc

srun -N 4 -n 8 --mpi=pmi2 ./mpi_hello_world
